// Time Complexicity : O(log N)

const num = -3;
const reverseInt = (num) => {
  const reversed = parseInt(num.toString().split("").reverse().join(""));
  return num > 0 ? reversed : reversed * -1;
};
console.log(reverseInt(num));
