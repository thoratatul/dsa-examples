// Time Complexicity : O(N)
// Space Complexicity : O(N)
const chunk = (array, size) => {
  let res = [];
  for (let i = 0; i < array.length; i++) {
    const item = array[i];
    const lastItem = res[res.length - 1];
    if (!lastItem || lastItem.length === size) {
      res.push([item]);
    } else {
      lastItem.push(item);
    }
  }
  return res;
};

console.log(chunk(["a", "b", "c", "d"], 2));
