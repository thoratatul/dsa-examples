// Time Complexicity : O(N)
// Space Complexicity : O(1)

const str = "I loveEeEeEe noodles";
const maxChar = (str) => {
  let charObj = {};
  let maxCount = 0;
  let maxChar = "";
  str = str.toString();
  for (let i = 0; i < str.length; i++) {
    const char = str[i];
    charObj[char] = charObj[char] + 1 || 1;
    if (charObj[char] > maxCount) {
      maxChar = char;
      maxCount = charObj[char];
    }
  }
  return maxChar;
};
console.log(maxChar(str));
