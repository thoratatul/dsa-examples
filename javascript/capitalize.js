// Time Complexicity : O(N)

const capitalize = (str) => {
  const strArray = str.split(" ");
  const arrOfWords = [];
  for (let i = 0; i < strArray.length; i++) {
    const word = strArray[i];
    arrOfWords.push(word[0].toUpperCase() + word.slice(1).toLowerCase());
  }
  return arrOfWords.join(" ");
};

console.log(capitalize("sHoRt AnD sToUt"));
