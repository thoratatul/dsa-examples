// Time Complexicity : O(N)

const str = "abcde";

// Reverse String Using Predefined Method i.e. array.reverse()
const reverseString1 = (str) => {
  return str.split("").reverse().join("");
};

// Reverse String Using For Loop
const reverseString2 = (str) => {
  let res = "";

  // Method 1
  //   for (let i = str.length - 1; i >= 0; i--) {
  //     res += str[i];
  //   }

  // Method 2
  for (let i = 0; i < str.length; i++) {
    res = str[i] + res;
  }

  return res;
};

// Reverse String Using Reduce
const reverseString3 = (str) => {
  return str.split("").reduce((output, char) => {
    return char + output;
  }, "");
};

// console.log(reverseString1(strng));
// console.log(reverseString2(strng));
console.log(reverseString3(str));
