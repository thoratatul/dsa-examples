// Big O Notation gives us a precise, numeric, and objective way of judging the perfomarmance of our code.

// O(N)
// const sumUpto = (n) => {
//   let total = 0;
//   for (let i = 1; i <= n; i++) {
//     total += i;
//   }
//   return total;
// };

// O(3)
const sumUpto = (n) => {
  return (n * (n + 1)) / 2;
};

console.log(sumUpto(3));
