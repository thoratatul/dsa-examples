// Time Complexicity : O(N)

const num = 15;

// Method 1
const fizzbuzz1 = (num) => {
  const numArray = [];
  for (let i = 1; i <= num; i++) {
    if (i % 3 === 0 && i % 5 === 0) {
      numArray.push("fizzbuzz");
    } else if (i % 3 === 0) {
      numArray.push("fizz");
    } else if (i % 5 === 0) {
      numArray.push("buzz");
    } else numArray.push(i);
  }
  return numArray;
};

// Method 2
const fizzbuzz2 = (num) => {
  const numArray = [];
  for (let i = 1; i <= num; i++) {
    let str = "";
    if (i % 3 === 0) {
      str += "fizz";
    }
    if (i % 5 === 0) {
      str += "buzz";
    }
    if (!str) {
      str = i;
    }
    numArray.push(str);
  }
  return numArray;
};

console.log(fizzbuzz2(num));
